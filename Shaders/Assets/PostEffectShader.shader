﻿Shader "Custom/PostEffectShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		blur_intensity_("Blur", Range(0,2)) = 1
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			float blur_intensity_;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed2 offset = fixed2(0,0.01)*blur_intensity_* sin(_Time.w);

				fixed4 s = tex2D(_MainTex, i.uv);
				fixed4 s1 = tex2D(_MainTex, i.uv + offset);
				fixed4 s2 = tex2D(_MainTex, i.uv - offset);

				return (s+s1+s2)/3;
				// just invert the colors
				//col = 1 - col;
			}
			ENDCG
		}
	}
}
