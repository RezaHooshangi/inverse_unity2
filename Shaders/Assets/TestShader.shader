﻿Shader "Custom/TestShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		tint_color_("Tint", Color) = (1,1,1,1)
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			fixed4 tint_color_;

			float plot(fixed2 uv,float f)
			{
				return smoothstep(f-0.05,f,uv.y) - smoothstep(f,f+0.05,uv.y);
			}

			fixed4 frag (v2f i) : SV_Target
			{
				return tex2D(_MainTex, i.uv) * tint_color_;

				fixed4 c1=fixed4(1,0,0,1);
				fixed4 c2=fixed4(0,1,0,1);

				float x = i.uv.x + _Time.y;
				float f1 = sin(x)/2 + 0.5;

				return c1* plot(i.uv,f1);
			}
			ENDCG
		}
	}
}
