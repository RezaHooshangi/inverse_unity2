﻿Shader "Custom/FlashbangShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		old_buffer_("Old Buffer", 2D) = "white" {}
		intensity_("Intensity",Range(0,1))= 0
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			sampler2D old_buffer_;
			float intensity_;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 current= tex2D(_MainTex, i.uv) * (1-intensity_);
				fixed4 old = tex2D(old_buffer_, i.uv) * intensity_;

				return current+old;
			}
			ENDCG
		}
	}
}
