﻿using UnityEngine;

public class PostEffectManager : MonoBehaviour
{
    [SerializeField]
    private Material material_;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, material_);
    }
}
