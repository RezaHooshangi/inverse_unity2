﻿Shader "Custom/SimpleColorGrading"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		source_color_("source_color_", Color) = (1,1,1,1)
		target_color_("target_color_", Color) = (1,1,1,1)
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			fixed4 source_color_;
			fixed4 target_color_;

			float plot(fixed2 uv,float f)
			{
				return smoothstep(f-0.05,f,uv.y) - smoothstep(f,f+0.05,uv.y);
			}

			fixed4 frag (v2f i) : SV_Target			
			{
				fixed4 texture_color= tex2D(_MainTex, i.uv);

				fixed4 diff = texture_color - source_color_;
				if(length(diff)<0.1)
					return target_color_;
				else
					return texture_color;
			}
			ENDCG
		}
	}
}
