﻿using UnityEditor;
using UnityEngine;

public class AssetManagerWindow : EditorWindow
{
    [MenuItem("Window/AssetManager")]
    private static void Show()
    {
        EditorWindow.GetWindow<AssetManagerWindow>();
    }

    private Object object_;
    private string filter_;
    private Color color_;

    private string prefab_path_;
    private string sprite_path_;
    private void OnGUI()
    {
        object_ = EditorGUILayout.ObjectField(object_, typeof(Object), true);
        if (object_ != null)
        {
            var instance_id = object_.GetInstanceID();
            GUILayout.Label("InstanceID: " + instance_id);

            var path = AssetDatabase.GetAssetPath(object_);
            GUILayout.Label("Path: " + path);

            var guid = AssetDatabase.AssetPathToGUID(path);
            GUILayout.Label("GUID: " + guid);
        }

        EditorGUILayout.Separator();
        filter_ = GUILayout.TextField(filter_);
        color_ = EditorGUILayout.ColorField(color_);

        if (GUILayout.Button("SetColor"))
        {
            DoAutomationOnAllSpriteRenderers(filter_, SetColorAction);
        }

        EditorGUILayout.Separator();

        prefab_path_ = GUILayout.TextField(prefab_path_);
        sprite_path_ = GUILayout.TextField(sprite_path_);
        if (GUILayout.Button("LoadPrefab"))
        {
            var prefab = Resources.Load<GameObject>(prefab_path_);
            if (prefab != null)
            {
                Debug.Log(prefab.name, prefab);
                var instance = Instantiate(prefab);

                var sprite = Resources.Load<Sprite>(sprite_path_);
                Debug.Log(sprite, sprite);

                var renderer = instance.GetComponent<SpriteRenderer>();
                renderer.sprite = sprite;
            }
            else
                Debug.LogError("Cant load resource.");
        }
    }

    private void SetColorAction(SpriteRenderer renderer)
    {
        renderer.color = color_;
        Debug.Log("Set color on " + renderer.gameObject.name, renderer);
    }

    private delegate void SpriteRendererAutomation(SpriteRenderer renderer);
    private void DoAutomationOnAllSpriteRenderers(string filter, SpriteRendererAutomation action)
    {
        var guids = AssetDatabase.FindAssets(filter);
        for (int i = 0; i < guids.Length; i++)
        {
            var guid = guids[i];
            var path = AssetDatabase.GUIDToAssetPath(guid);
            var obj = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject));
            var prefab = obj as GameObject;
            if (prefab != null)
            {
                var renderers = prefab.GetComponentsInChildren<SpriteRenderer>();
                for (int j = 0; j < renderers.Length; j++)
                {
                    var renderer = renderers[j];
                    action(renderer);
                }
            }

            Resources.UnloadUnusedAssets();
        }
    }
}
