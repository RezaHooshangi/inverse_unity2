﻿using UnityEngine;
using System.Collections;

public class PrefabDrawerAttribute : PropertyAttribute
{
    public Color pColor { get; private set; }
    public PrefabDrawerAttribute(float r, float g, float b)
    {
        pColor = new Color(r, g, b);
    }
}

[System.Serializable]
public class PrefabData
{
    [SerializeField]
    private string prefab_guid_;
}

public class PrefabContainerComponent : MonoBehaviour
{
    [SerializeField]
    [PrefabDrawer(1f, 0f, 0f)]
    public string prefab_guid_;

    [SerializeField]
    [PrefabDrawer(0f, 1f, 0f)]
    public string prefab_guid_1;

    [SerializeField]
    [PrefabDrawer(0f, 0f, 1f)]
    public string prefab_guid_2;

    [SerializeField]
    [PrefabDrawer(1f, 1f, 1f)]
    public string prefab_guid_3;

    [SerializeField]
    private PrefabData prefab_;
    //[SerializeField]
    //private GameObject prefab_;
}
