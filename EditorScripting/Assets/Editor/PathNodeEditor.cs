﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PathNode))]
public class PathNodeEditor : Editor
{
	private void OnSceneGUI()
	{
		var node = target as PathNode;
		DrawPathNode (node);
	}

	public static void DrawPathNode(PathNode node)
	{
		Handles.DrawWireArc (node.transform.position, node.transform.up, node.transform.forward,360,node.radius_);

		var size = HandleUtility.GetHandleSize (node.transform.position);
		//node.radius_ =	Handles.ScaleValueHandle (node.radius_, node.transform.position + node.transform.forward * node.radius_, node.transform.rotation, 5, Handles.ArrowHandleCap, 1);
		node.radius_ = Handles.ScaleValueHandle(node.radius_, node.transform.position+node.transform.forward*node.radius_, Quaternion.identity, size, Handles.ConeHandleCap, 1);
		//node.radius_= Handles.RadiusHandle (node.transform.rotation, node.transform.position, node.radius_);

		var size2 = HandleUtility.GetHandleSize (node.transform.position)*0.1f;
		//node.transform.position=	Handles.DoPositionHandle (node.transform.position, node.transform.rotation);
		node.transform.position=	Handles.FreeMoveHandle (node.transform.position, node.transform.rotation, size2, Vector3.one, Handles.DotHandleCap);
	}
}
