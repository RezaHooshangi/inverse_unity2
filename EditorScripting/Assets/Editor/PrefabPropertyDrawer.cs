﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(PrefabDrawerAttribute))]
public class PrefabPropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var attrib = attribute as PrefabDrawerAttribute;

        string current_guid = property.stringValue;
        string current_path = AssetDatabase.GUIDToAssetPath(current_guid);
        GameObject current_prefab = (GameObject)AssetDatabase.LoadAssetAtPath(current_path, typeof(GameObject));

        GUI.color = attrib.pColor;

        EditorGUI.BeginChangeCheck();
        GameObject new_prefab = (GameObject)EditorGUI.ObjectField(position, current_prefab, typeof(GameObject), true);
        if (EditorGUI.EndChangeCheck())
        {
            string new_path = AssetDatabase.GetAssetPath(new_prefab);
            string new_guid = AssetDatabase.AssetPathToGUID(new_path);

            property.stringValue = new_guid;
        }
    }
}