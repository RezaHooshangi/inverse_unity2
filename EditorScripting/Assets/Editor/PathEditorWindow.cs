﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PathEditorWindow : EditorWindow
{
	[MenuItem("Window/Path Editor")]
	private static void ShowWindow()
	{
		GetWindow<PathEditorWindow> ("Path Editor");
	}

	private void OnEnable()
	{
		nodes_ = new List<PathNode> ();
		RefreshNodes ();

		SceneView.onSceneGUIDelegate += OnSceneGUI;
	}
	private void OnDisable()
	{
		SceneView.onSceneGUIDelegate -= OnSceneGUI;
	}

	private void OnSceneGUI(SceneView scene_view)
	{
		Handles.BeginGUI ();

		if (GUILayout.Button ("Log"))
			Debug.Log ("Log");

		Handles.EndGUI ();

		for (int i = 0; i < nodes_.Count; i++) 
		{
			var node = nodes_ [i];

			Handles.Label (node.transform.position + node.transform.up * 5f, i.ToString ());
			
			PathNodeEditor.DrawPathNode (node);

			if(i<nodes_.Count-1)
			{
				var next_node = nodes_ [i + 1];
				Handles.DrawLine (node.transform.position, next_node.transform.position);

				var arrow_control_id = GUIUtility.GetControlID (FocusType.Passive);
				var arrow_position=(node.transform.position+next_node.transform.position)/2f;
				var arrow_rotation = (next_node.transform.position - node.transform.position);
				Handles.ArrowHandleCap (arrow_control_id, arrow_position, Quaternion.LookRotation (arrow_rotation), 5, EventType.repaint);
			}
		}

		var unit = 1f / (nodes_.Count-1);
		var start_index = Mathf.FloorToInt(value_ / unit);
		var mode = value_ - (start_index * unit);
		var fraction = mode / unit;

		Vector3 current = nodes_ [start_index].transform.position;
		Vector3 next= (start_index ==nodes_.Count-1)?nodes_[start_index].transform.position:nodes_[start_index+1].transform.position;		

		var position = Vector3.Lerp (current, next, fraction);

		Handles.DrawSolidDisc(position, Vector3.one, 2);
		SceneView.RepaintAll ();
	}

	private List<PathNode> nodes_;
	private void RefreshNodes()
	{
		nodes_.Clear ();
		var nodes = FindObjectsOfType<PathNode> ();
		for (int i = 0; i < nodes.Length; i++)
			nodes_.Add (nodes [i]);
	}

	private float value_;
	private void OnGUI()
	{
		if (GUILayout.Button ("Find Nodes"))
			RefreshNodes ();
		
		for (int i = 0; i < nodes_.Count; i++) 
		{
			var node = nodes_ [i];

			nodes_[i]=(PathNode)EditorGUILayout.ObjectField (node, typeof(PathNode), true);
		}

		if (GUILayout.Button ("Add")) 
		{
			nodes_.Add (null);
		}

		value_ = EditorGUILayout.Slider (value_, 0f, 1f);



		//Debug.Log (start_index+" / "+fraction);


		//GUILayout.Label (start_index+" / "+fraction);
	}
}
