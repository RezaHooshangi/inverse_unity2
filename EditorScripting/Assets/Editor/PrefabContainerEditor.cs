﻿using UnityEngine;
using UnityEditor;

//[CustomEditor(typeof(PrefabContainerComponent))]
public class PrefabContainerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        PrefabContainerComponent comp = target as PrefabContainerComponent;

        string current_guid = comp.prefab_guid_;
        string current_path = AssetDatabase.GUIDToAssetPath(current_guid);
        GameObject current_prefab = (GameObject)AssetDatabase.LoadAssetAtPath(current_path, typeof(GameObject));

        EditorGUI.BeginChangeCheck();
        GameObject new_prefab = (GameObject)EditorGUILayout.ObjectField(current_prefab, typeof(GameObject), true);
        if (EditorGUI.EndChangeCheck())
        {
            string new_path = AssetDatabase.GetAssetPath(new_prefab);
            string new_guid = AssetDatabase.AssetPathToGUID(new_path);

            Undo.RecordObject(comp, "SetPrefab");
            comp.prefab_guid_ = new_guid;
            EditorUtility.SetDirty(comp);
        }
    }
}