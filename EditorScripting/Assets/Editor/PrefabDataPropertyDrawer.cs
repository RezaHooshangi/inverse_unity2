﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer(typeof(PrefabData))]
public class PrefabDataPropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var guid_property = property.FindPropertyRelative("prefab_guid_");

        string current_guid = guid_property.stringValue;
        string current_path = AssetDatabase.GUIDToAssetPath(current_guid);
        GameObject current_prefab = (GameObject)AssetDatabase.LoadAssetAtPath(current_path, typeof(GameObject));

        EditorGUI.BeginChangeCheck();
        GameObject new_prefab = (GameObject)EditorGUI.ObjectField(position, current_prefab, typeof(GameObject), true);
        if (EditorGUI.EndChangeCheck())
        {
            string new_path = AssetDatabase.GetAssetPath(new_prefab);
            string new_guid = AssetDatabase.AssetPathToGUID(new_path);

            guid_property.stringValue = new_guid;
        }
    }
}
